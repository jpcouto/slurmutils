#!/usr/bin/env python
# Install script for SLURMutils
# Joao Couto - Jan 2019

from setuptools import setup
from setuptools.command.install import install


longdescription = '''High-level utilities to submit jobs that are similar and track submitted jobs to a SLURM cluster.'''
setup(
    name = 'slurmutils',
    version = '0.0',
    author = 'Joao Couto',
    author_email = 'jpcouto@gmail.com',
    description = (longdescription),
    long_description = longdescription,
    license = 'GPL',
    packages = ['slurmutils'],
    entry_points = {
        'console_scripts': [
#            'backup-experiment = copyutils.copy:tobackup',
        ]
    },
)
