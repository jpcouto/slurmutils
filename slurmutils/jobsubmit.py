import sys
import os
import shutil
import subprocess as sub
from glob import glob
import numpy as np
import time
from os.path import join as pjoin
import re


def parse_parameter_slurm_jobs(par):
    tobejobs = np.sort([s for s in filter(lambda x: x.isnumeric(),
                                          par['slurm'].keys())])
    jobs = []
    for j in tobejobs:
        try:
            i = int(j)
            jobs.append(j)
        except:
            pass
    return jobs

def submit_slurm_jobs(filename,jobs = None):
    par = read_parameters_file(filename)
    if jobs is None:
        jobs = parse_parameter_slurm_jobs(par)
    slurmid = []
    for job in jobs:
        jobdesc = par['slurm'][job]
        sbatch_append = ''
        if 'dependency' in jobdesc.keys():
            if not 'dependency-key' in jobdesc.keys():
                jobdesc['dependency-key'] = 'afterok'
            idx  = jobdesc['dependency']
            sbatch_append = '--dependency={1}:{0}'.format(slurmid[idx],jobdesc['dependency-key'])
        if 'extra_sbatch' in jobdesc.keys():
            sbatch_append += ' {0}'.format(jobdesc['extra_sbatch'])
        fname = pjoin(par['scratch_data_path'],'sjob_{0:03d}.sh'.format(int(job)))
        jobdesc['command'] = 'cd ' + par['scratch_data_path'] + '\n' + jobdesc['command']
        jobid = create_slurm_job(filename = fname, sbatch_append = sbatch_append, **jobdesc)
        if not jobid is None:
            slurmid.append(jobid)
            par['slurm'][job]['jobid'] = jobid
            print('Filename:' + fname)
        else:
            print('Could not submit '+fname+ '. giving up...')
            sys.exit(1)
    write_parameters_file(filename,par,overwrite=True)
    nerfpath = pjoin(os.path.expanduser('~'), 'nerf-cluster')
    if not os.path.isdir(nerfpath):
        os.makedirs(nerfpath)
    with open(pjoin(nerfpath,'slurm_user.log'),'a') as f:
        from datetime import datetime
        dd = datetime.now()
        f.write('{0} -> {1}\n'.format(dd.strftime('%y/%m/%d %H:%M:%S'),filename))

############################################################################
        
def create_slurm_job(filename,
                     command='echo "Hello world!" \n sleep 10',
                     jobname='job_doe',ntasks=1,
                     ncpuspertask = 1,
                     memory=None,
                     walltime=None,partition=None,
                     conda_environment=None,
                     module_environment=None,
                     mail=None,
                     sbatch_append='',**kwargs):
    sjobfile = '''#!/bin/bash -login
#SBATCH --job-name={0}
#SBATCH --output=log_%j.out
#SBATCH --error=log_%j.err

#SBATCH --ntasks={1}
#SBATCH --cpus-per-task={2}
'''.format(jobname,ntasks,ncpuspertask)
    if not walltime is None:
        sjobfile += '#SBATCH --time={0} \n'.format(walltime)
    if not memory is None:
        sjobfile += '#SBATCH --mem={0} \n'.format(memory)
    if not partition is None:
        sjobfile += '#SBATCH --partition={0} \n'.format(partition)
    if not mail is None:
        sjobfile += '#SBATCH --mail-user={0} \n#SBATCH --mail-type=END,FAIL \n'.format(mail)
    if not module_environment is None:
        sjobfile += '\n module purge\n'
        sjobfile += '\n module load {0} \n'.format(module_environment)
    if not conda_environment is None:
        sjobfile += 'source activate {0} \n'.format(conda_environment)
    sjobfile += '''echo JOB STARTED `date`
{0}
echo JOB FINISHED `date`
'''
    if os.path.isfile(filename):
        print('File exists.')
    with open(filename,'w') as f:
        f.write(sjobfile.format(command))
    folder,fname = os.path.split(filename)
    submit_cmd = 'cd {0} && sbatch {2} {1}'.format(folder,fname,sbatch_append)
    proc = sub.Popen(submit_cmd, shell=True, stdout=sub.PIPE)
    out,err = proc.communicate()
    
    if b'Submitted batch job' in out:
        jobid = int(re.findall("Submitted batch job ([0-9]+)", str(out))[0])
        return jobid
    else:
        return None
