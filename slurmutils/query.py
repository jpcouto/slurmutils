import re

def parse_to_str_list(stdout,append = True):
    res = []
    for i,l in enumerate(stdout):
        if append:
            res.append(re.split('\s+', l.strip('\n').strip(' ')))
        else:
            res.extend(re.split('\s+', l.strip('\n').strip(' ')))
    return res

def parse_squeue(stdout):
    res = parse_to_str_list(stdout)
    if len(res) > 1:
        return [dict(jobid = r[0],
                     name=r[1],
                     partition = r[2],
                     user = r[3],
                     status = r[4],
                     time = r[5],
                     nodes = r[6],
                     cpus = r[7],
                     nodelist = r[8]
                     ) for r in res[1:]]
    else:
        return []
    

def parse_jobinfo(stdout):
    res = parse_to_str_list(stdout,append = False)
    resdict = {}
    for r in res:
        r = r.split('=')
        if len(r)>1:
            resdict[r[0].lower()] = r[1]
    return resdict

