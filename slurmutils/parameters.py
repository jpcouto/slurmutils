#!/usr/bin/env python
# Function and scripts to read and edit parameter files
import os
import json
import numpy as np
import re
from glob import glob
from os.path import join as pjoin
from os.path import isfile,isdir,abspath,expanduser
import copy
import subprocess as sub
import six

################################################################################
#######################PARAMETERS AND DEFAULTS##################################
################################################################################
# File format dependent extensions for different recording systems
preferencepath = pjoin(expanduser('~'), '.copyutils')

defaultpreferences = dict(mandatory_keys = ['input_data_path',
                                            'scratch_data_path',
                                            'output_data_path',
                                            'slurm'])
examplefile = dict(input_data_path = '~/tmp',
                   scratch_data_path = '~/tmp/test',
                   output_data_path = '~/tmp/test',
                   slurm = {'0':dict(
                       command = 'time && ls {input_data_path}',
                       jobname = 'job_doe',
                       ntasks = 1,
                       ncpuspertask = 1,
                       memory = None,
                       partition = 'short',
                       conda_environment = 'py3',
                       module_environment = 'conda')})

################################################################################
###########################SUPPORT FUNCTIONS####################################
################################################################################
def _find_parenthesis(string):
    '''Searches for things between parentheses'''
    if isinstance(string,six.string_types):
        keys = [k.strip('{').strip('}') for k in re.findall('\{.*?\}',string)]
        return keys
    else:
        return []
    
def _strip_keys(par):
    '''Recursive search for keys in dictionary items'''
    res = []
    for k in par.keys():
        v = par[k]
        if type(v) is dict:
            res += _strip_keys(v)
        else:
            res +=_find_parenthesis(v)
    return [r for r in np.unique(res)]

def _find_key_value(key,par):
    '''Recursively append key values'''
    res = []
    for k in par.keys():
        v = par[k]
        if type(v) is dict:
            res += _find_key_value(key,v)
        else:
            if k == key:
                res += [v]
    return res

def _fill_enviroment_variables(dictio):
    '''Fill environment variables in dictionary.'''
    for k in dictio.keys():
        if '$' in k:
            dictio[k] = [os.environ[k.strip('$')]]

def _replace_values_in_keys(keydict,dictio,doslurm=False):
    '''Replace keys in dictionary with items from keydictionary. 
    if doslurm is False the slurm section is skipped.
    '''
    for k in dictio.keys():
        v = dictio[k]
        if type(v) is dict:
            if k == 'slurm' and not doslurm:
                pass
            else:
                dictio[k] = _replace_values_in_keys(keydict,v,doslurm)
        keys = _find_parenthesis(v)
        if len(keys):
            try:
                dictio[k] = v.format(**keydict)
            except:
                print('Could not replace a key:'+k)
    return dictio

def _recursive_replace_dict(key, nkey,dictio):
    '''Replace values in dictionary with new key'''
    if not type(dictio) is dict:
        print(dictio)
    for k in dictio.keys():
        v = dictio[k]
        if type(v) is dict:
            _recursive_replace_dict(key,nkey,dictio[k])
        if isinstance(v,six.string_types):
            if key in v:
                dictio[k] = v.replace(key,nkey)

def _validate_keys(keydict):
    '''Check that all values are unique and not empty.'''
    duplicates = []
    emptykeys = []
    for k in keydict.keys():
        v = keydict[k]
        if len(v)>1:
            duplicates += [k]
        if len(v) == 0 and not '$' in k:
            emptykeys += [k]
    if len(emptykeys):
        print('There are tags that are not defined: '+','.join(emptykeys))

################################################################################
################################################################################

def write_parameters_file(destpath, param, overwrite=False):
    '''
Parameter files contain the description of:
    - data location
    - order and which slurm jobs to run
    '''

    if not overwrite:
        assert not isfile(destpath), "File "+destpath+" exists, delete it to update."

    with open(destpath, 'w') as outfile:
        json.dump(param, outfile, sort_keys = True, indent = 4)
    
            
def read_parameters_file(parfile):
    '''
    Reads a json submission or parsed submission file. Returns a dictionary of parameters.
    '''
    assert isfile(parfile), "File " +parfile+ " not found."
    with open(parfile, 'r') as infile:
        param = json.load(infile)
    return param

def parse_parameters(param, reserved_keys = [], ignore_reserved = True, doslurm=False):
    '''
    Parses parameters from a submission file dictionary.
    You can generate one with read_parameters_file(jsonfile)
    Validates file.
    '''
    # identify keys
    keys = np.unique(_strip_keys(param))
    # build a dictionary of keys
    
    keydict = {} #reserved_keysdict.copy()
    for k in keys:
        if not k in reserved_keys or not ignore_reserved:
            keydict[k] = _find_key_value(k,param)
    # Check that no tags are missing
    _validate_keys(keydict)
    _fill_enviroment_variables(keydict)
    for k in keydict.keys():
        v = keydict[k]
        keydict[k] = v[0]
    keydict = _replace_values_in_keys(keydict,keydict.copy(),doslurm)
    values = _replace_values_in_keys(keydict,param.copy(),doslurm)
    return values

def prepare_parsed_parameters(param,preferences=defaultpreferences):
    '''
    prepare_parsed_parameters(param)
    Parses the parameter file, prepares folders and generate parsed parameter file.
    '''
    # Replace variables on file.
    par = parse_parameters(param)
    # Run this twice to be sure to replace all keys with the key replacement...
    par = parse_parameters(par.copy())
    ##############################################
    filesforsubmission = []
    # Are the mandatory keys specified?
    hasincrement = False
    inc = 0
    scratchinc = 0
    for key in preferences['mandatory_keys']:
        assert key in par.keys(), key + ' is mandatory, please add it to the parameter file.'
        if key == 'input_data_path':
            # Is the raw data there?
            assert isdir(expanduser(par['input_data_path'])), 'Input data folder does not exist:' + par['input_data_path']
            files_in_input = np.sort(glob(pjoin(expanduser(par['input_data_path']),'*')))
            assert len(files_in_input), "Input data folder seems to be empty."
            # Prepare output folder and scratch.
            # Handle increments if present.
            if key == 'output_data_path':
                if ('[increment]' in par['output_data_path']):
                    # Test if output folders exist.
                    tmp = par['output_data_path'].replace('[increment]','{0:03d}')
                    while isdir(expanduser(tmp.format(inc))):
                        inc += 1
                        print("    Found session on output_data_path:" + os.path.basename(tmp.format(inc)))
                    hasincrement = True

            if key == 'scratch_data_path':
                if ('[increment]' in par['scratch_data_path']):
                    # Test in scratch
                    tmp = par['scratch_data_path'].replace('[increment]','{0:03d}')

                    while isdir(expanduser(tmp.format(scratchinc))):
                        scratchinc += 1
                        print("    Found session on scratch_data_path:" + os.path.basename(tmp.format(scratchinc)))
                    hasincrement = True

    if hasincrement:
        inc = max([inc,scratchinc])
        par['session_number'] = inc
        _recursive_replace_dict('[increment]', '{0:03d}'.format(inc),par)
    # Now create scratch and copy files

    output_data_path = expanduser(par['output_data_path'])
    scratch_data_path = expanduser(par['scratch_data_path'])
    if not isdir(scratch_data_path):
        os.makedirs(scratch_data_path)
    if 'experiment_name' in par.keys():
        expname = par['experiment_name']
        bname = expname
    else:
        assert False, "\n\nSubmission files with no experiment_name are not implemented yet."
    if 'session_number' in par.keys():
        bname += 'session_{1:03d}'.format(par['session_number'])
    par = parse_parameters(par.copy(),ignore_reserved=False, doslurm=True)
    fileforsubmission = pjoin(scratch_data_path,bname+'.json')
    # Write the parameter file.
    write_parameters_file(fileforsubmission,par,True)
    return [fileforsubmission]

################################################################################
####################################SCRIPTS#####################################
################################################################################


def parse_and_submit_file():
    from argparse import ArgumentParser
    parser = ArgumentParser(description='Submit a job to the cluster from a submission file.')
    parser.add_argument('fname',metavar='filename',
                        type=str,help='submission file name or path',
                        nargs=1)
    parser.add_argument('--no-submit',
                        action='store_true',
                        help="don't submit the job, prepare only",
                        default=False)

    opts = parser.parse_args()
    if isfile(opts.fname[0]):
        param = read_parameters_file(opts.fname[0])
        jobtosort = prepare_parsed_parameters(param)
        if not jobtosort is None and not opts.no_submit:
            from .job_submit import submit_slurm_job
            for job in jobtosort:
                submit_slurm_job(job)
    else:
        print("File : {0} not there? Check path.".format(opts.fname[0]))

